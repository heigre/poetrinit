# Poetrinit
Initiates a poetry project in an existing directory by using a dockerized poetry installation.

## Requirements
```
Docker, Docker Compose
```

## Configuration
Edit the volume binding in ```Dockerfile.poetrinit``` to mount the 
project folder you intend to initiate as a poetry project.


## Build and Run
```
docker compose up
```

## Authors and acknowledgment
Mats Heigre

## License
MIT

## Project status
